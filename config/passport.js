
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
passport.serializeUser((user, done) => {
    console.log('serializeUser', user.email)
    done(null, user);
})

passport.deserializeUser((user, done) => {
    done(null, user)
})


passport.use('local-signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    console.log('=>: email', email)

    User.findOne({ username: email}, function (err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    });
}));

