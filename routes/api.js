var passport = require('passport');
var express = require('express');
var router = express.Router();
var User = require("../models/user");
var Book = require("../models/book");

router.get('/logout', (req, res, next) => {
  console.log('Cerrando sesión');
  req.logout();
  res.redirect('/login')
});

router.post('/signup', function (req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, msg: 'Ingresa usuario y contraseña' });
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password
    });
    newUser.save(function (err) {
      if (err) {
        return res.json({ success: false, msg: 'El usuario ya esta registrado' });
      }
      res.json({ success: true, msg: 'Usuario resgistado correctamente' });
    });
  }
});

router.post('/signin', (req, res, next) => { console.log(req.body); next() },
  passport.authenticate('local-signin', {
    passReqToCallback: true
  }), (req, res, next) => {
    console.log('=>: req.user', req.user)
    req.login(req.user, (err) => {
      req.logIn(req.user, (err) => {
        if (err) return res.status(401).send({
          message: 'No se pudo iniciar sesión'
        })
        console.log(err)
        if (err) return res.status(401).send({
          message: 'No se pudo iniciar sesión'
        })
        res.send(req.user)
      })
    })
  });

router.post('/book', isLogin, function (req, res) {

  console.log(req.body);
  var newBook = new Book({
    isbn: req.body.isbn,
    title: req.body.title,
    author: req.body.author,
    publisher: req.body.publisher
  });

  newBook.save(function (err) {
    if (err) {
      return res.json({ success: false, msg: 'Error al guardar el libro' });
    }
    res.json({ success: true, msg: 'Se guardó el libro!' });
  });

});

router.get('/book', isLogin, function (req, res) {
  Book.find(function (err, books) {
    if (err) return next(err);
    res.json(books);
  });
});

router.get('/users', (req, res) => {
  User.find({}, (err, docs) => {
    if (err) return next(err)
    res.send({
      message: 'Esta ruta no está protegida, puedes ver los usuarios sin iniciar sesión',
      data: docs
    })
  })
})

router.get('/protected/users',isLogin, (req, res) => {
  User.find({}, (err, docs) => {
    if (err) return next(err)
    res.send({
      message: 'Esta ruta está protegida, solo se ve si hay sesión',
      data: docs
    })
  })
})

router.use((err, req, res, next) => {
  res.status(500).send({
    err: error.message
  })
})

function isLogin(req, res, next) {
  if (req.isAuthenticated()) return next()

  res.status(401)
    .send({
      message: 'Unauthorized'
    })
}

module.exports = router;
