#  (Angular 5) Web Application usando Passport



Instrucciones:

* Ejecutar el server de mongodb
* Ejecutar `npm install`
* Ejecutar `npm install --save-dev @angular-devkit/core`
* Ejecutar `npm start`


API:
    * /api/users : Ruta no protegida, se puede ver sin haber iniciado sesión
    * /api/protected/users : Ruta protegida, solo se ve si el usuario tiene sesión

